/*
 * 
 * Copyright 2014 Jules White
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package org.magnum.mobilecloud.video;

import java.security.Principal;
import java.util.Collection;
import java.util.Set;
import javax.servlet.http.HttpServletResponse;
import org.magnum.mobilecloud.video.client.VideoSvcApi;
import org.magnum.mobilecloud.video.repository.Video;
import org.magnum.mobilecloud.video.repository.VideoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.common.collect.Lists;

@Controller
public class VideoController {
	
	@Autowired
    private VideoRepository videos;
	
	@RequestMapping(value="/go",method=RequestMethod.GET)
	public @ResponseBody String goodLuck(){
		return "Good Luck!";
	}
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoList(){
		return Lists.newArrayList(videos.findAll());
	}
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH, method=RequestMethod.POST)
	public @ResponseBody Video addVideo(@RequestBody Video v){
		return videos.save(v);
	}
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH + "/{id}", method=RequestMethod.GET)
	public @ResponseBody Video getVideo(@PathVariable("id") long id, HttpServletResponse response) {
		Video video = videos.findById(id);
		if (video != null) {
			return video;
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
	}
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH + "/{id}/like", method=RequestMethod.POST)
	public @ResponseBody void likeVideo(@PathVariable("id") long id, Principal p, HttpServletResponse response) {
		Video video = videos.findById(id);
		if (video != null) {
			Set<String> users = video.getUsers();
			if (users.add(p.getName())) {
				long likes = video.getLikes();
				likes++;
				video.setLikes(likes);
				videos.save(video);
			} else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH + "/{id}/unlike", method=RequestMethod.POST)
	public @ResponseBody void unLikeVideo(@PathVariable("id") long id, Principal p, HttpServletResponse response) {
		Video video = videos.findById(id);
		if (video != null) {
			Set<String> users = video.getUsers();
			if (users.remove(p.getName())) {
				long likes = video.getLikes();
				likes--;
				video.setLikes(likes);
				videos.save(video);
			} else {
				response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
			}
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
		}
	}
	
	@RequestMapping(value=VideoSvcApi.VIDEO_SVC_PATH + "/{id}/likedby", method=RequestMethod.GET)
	public @ResponseBody Collection<String> listLikedBy(@PathVariable("id") long id, HttpServletResponse response) {
		Video video = videos.findById(id);
		if (video != null) {
			return video.getUsers();
		} else {
			response.setStatus(HttpServletResponse.SC_NOT_FOUND);
			return null;
		}
	}

	@RequestMapping(value=VideoSvcApi.VIDEO_TITLE_SEARCH_PATH, params = { "title" }, method=RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoListByNameContaining(@RequestParam(value = "title") String title, HttpServletResponse response){
		Collection<Video> matchingVideos = videos.findByNameContaining(title);
		if (matchingVideos != null) {
			return Lists.newArrayList(matchingVideos);
		} else {
			response.setStatus(HttpServletResponse.SC_OK);
			return Lists.newArrayList();
		}
	}
	
	@RequestMapping(value=VideoSvcApi.VIDEO_DURATION_SEARCH_PATH, params = { "duration" }, method=RequestMethod.GET)
	public @ResponseBody Collection<Video> getVideoListByDurationLessThan(@RequestParam(value = "duration") long duration, HttpServletResponse response){
		Collection<Video> matchingVideos = Lists.newArrayList(videos.findByDurationLessThan(duration));
		if (matchingVideos != null) {
			return Lists.newArrayList(matchingVideos);
		} else {
			response.setStatus(HttpServletResponse.SC_OK);
			return Lists.newArrayList();
		}
	}
}
