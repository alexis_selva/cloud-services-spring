package org.magnum.mobilecloud.video.repository;

import java.util.Collection;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VideoRepository extends CrudRepository<Video, Long>{

        // Find all videos
        public Collection<Video> findAll();
        
        // Find a video matching an id
        public Video findById(long id);
        
        // Find all videos with a containing t (e.g., Video.name)
        public Collection<Video> findByNameContaining(String title);
        
        // Find all videos with a duration lower than d title (e.g., Video.duration)
        public Collection<Video> findByDurationLessThan(long d);
}
