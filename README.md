These are the programming assignments relative to cloud services (SPRING):

* Assignment 1: creating a very basic application for uploading video to a cloud service and managing the video's metadata
* Assignment 2: building on the ideas in the original video service (assignment 1) to add OAuth 2.0 authentication of clients and the ability to "like" videos

For more information, I invite you to have a look at https://www.coursera.org/course/mobilecloud